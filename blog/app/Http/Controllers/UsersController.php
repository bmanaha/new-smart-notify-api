<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UsersController extends Controller
{
    public function index()
    {
    	//database data
    	$users = DB::table('users')->get();
        $json = json_encode($users);
        return($json);
        /*
        foreach ($users as $user)
        {
            var_dump($user); //skriver rent objekt ud
                                                        //var_dump($user->name);
                                                        //var_dump($user->email);         
        }
        */
        
        /*
        *$users = DB::select('select * from users where id = 1', array(1));
        *return view('admin.users.index', ['users' => $users]);
        */
        // hard coded data
	/* 	$users = [
		'0' => [
		'first_name' => 'Joe',
		'last_name' => 'Joe',
		'address' => 'some street'
		],
		'1' => [
		'first_name' => 'Lolita',
		'last_name' => 'Doll',
		'address' => 'Pusher Street'
		]
		
	];
	return view('admin.users.index', compact('users'));
	*/
    }
    public function create()
    {
    	return view('admin.users.create');
    }
    public function store(Request $request)
    {
    	User::create($request->all());
    	return 'Success';
    	return $request->all();
    }
}
