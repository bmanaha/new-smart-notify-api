<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'senderName', 'title', 'message', 'reciever',
    ];
    }
public function setSenderNameAttribute($value){
    $this->atributes['senderName'] = ucfirst($value);
}
    /*

public function setPasswordAttribute($value){
    $this->attributes['password'] = bbrypt($value);
}
public function getNameAttribute($value){
    return strtoupper($value);
}
*/