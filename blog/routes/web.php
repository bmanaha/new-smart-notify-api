<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
	//return realpath(base_path('resouces/views'));
    return view('welcome');
});
//Route::post('users', ['uses' => 'UsersController@store']);// original
//Route::get('users', ['uses' => 'UsersController@index']); //original
Route::get('users', 'UsersController@index')->middleware('cors'); //do not use since we want cors on all requests anyway
Route::get('users/create', ['uses' => 'UsersController@create']);
Route::post('users', 'UsersController@store')->middleware('cors'); //have to use middleware cors, else it won't be able to show up in another web application due to cross origin
// https://www.youtube.com/watch?v=-LJpDrLFiiU&list=PL3ZhWMazGi9IYymniZgqwnYuPFDvaEHJb&index=20 + https://www.youtube.com/watch?v=84bXch-YIvI

Route::get('api/messages', ['uses' => 'MsgsController@allmsg']);
Route::post('api/messages/create', ['uses' => 'msgsController@createMsg']);





// this is very early static code
/*
Route::get('users', function() {
	
	$users = [
		'0' => [
		'first_name' => 'Joe',
		'last_name' => 'Joe',
		'address' => 'some street'
		],
		'1' => [
		'first_name' => 'Lolita',
		'last_name' => 'Doll',
		'address' => 'Pusher Street'
		]
	];
	return $users;
});
*/

